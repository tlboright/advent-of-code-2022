fn calc_rock_score(expected_action: &str) -> i32 {
    match expected_action {
        "X" => 3,
        "Y" => 4,
        "Z" => 8,
        _ => 0
    }
}
fn calc_scissors_score(expected_action: &str) -> i32 {
    match expected_action {
        "X" => 2,
        "Y" => 6,
        "Z" => 7,
        _ => 0
    }
}
fn calc_paper_score(expected_action: &str) -> i32 {
    match expected_action {
        "X" => 1,
        "Y" => 5,
        "Z" => 9,
        _ => 0
    }
}
fn main() {
    let str_vec =
         include_str!("../input.txt")
        .lines()
        .collect::<Vec<&str>>();

    let mapped_strs = str_vec.iter().map(|val| val.split(" ").collect::<Vec<&str>>());
    let mut ans = 0;
    for s in mapped_strs {
        let opponent = s[0];
        let strategy = s[1];

        let cur = match opponent {
            "A" => calc_rock_score(strategy),
            "B" => calc_paper_score(strategy),
            "C" => calc_scissors_score(strategy),
            _ => 0
        };

        ans += cur;
    }

    println!("{}", ans);
}
