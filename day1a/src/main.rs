fn main() {
    let str_vec =
         include_str!("../input.txt")
        .lines()
        .collect::<Vec<&str>>();
    
    let mut s = 0;
    let end = str_vec.len() - 1;
    let mut max = 0;
    let mut curr = 0;
    loop {
        if s > end {
            break;
        }
        let my_str = str_vec.get(s).unwrap();
        if let Ok(result) = my_str.parse::<usize>() {
            println!("{}", result);
            curr += result;
        } else {
            max = std::cmp::max(curr, max);
            curr = 0;
        }
        s += 1;
    }

    println!("max: {}", max);
}
