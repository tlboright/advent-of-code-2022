fn main() {
    let str_vec =
         include_str!("../input.txt")
        .lines()
        .collect::<Vec<&str>>();
    

    let mut s = 0;
    let end = str_vec.len() - 1;
    let mut max1 = 0;
    let mut max2 = 0;
    let mut max3 = 0;
    let mut curr = 0;
    loop {
        if s > end {
            break;
        }
        let my_str = str_vec.get(s).unwrap();
        match my_str.parse::<usize>() {
            Ok(result) => curr += result,
            Err(_) => {
                if curr > max1 {
                    max3 = max2;
                    max2 = max1;
                    max1 = curr;
                } else if curr > max2 {
                    max3 = max2;
                    max2 = curr;
                } else {
                    max1 = std::cmp::max(curr, max1);
                }
                curr = 0;
            }
        }
        
        s += 1;
    }

    println!("max: {}", max1 + max2 + max3);
}
